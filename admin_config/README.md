## ASC IT Administrator - Configuring batch sync for Teams OneDrive

Included in this repository is an example batch sync file [sync_script_example.bat](./sync_script_example.bat)
- This is for Windows batch script using the `robocopy` command (more info [here](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/robocopy))...

The sample .bat contains examples for configuring both types of transfers, make sure to uncomment the command (remove double ":") and finish the script with `EXIT`:

```batch
::	 To copy FROM the ASC Network TO Teams OneDrive (for archiving to teams)
:: robocopy \\asc.upenn.edu\path\to\your\network_share "\\{NX_VMNAME}\C$\Users\{JANUS_USERNAME}\PennO365\{TEAM NAME & CHANNEL}" /MIR
	
:: 	To copy FROM Teams OneDrive TO the ASC network (for backups on prem))
:: robocopy "\\{NX_VMNAME}\C$\Users\{JANUS_USERNAME}\PennO365\{TEAM NAME & CHANNEL}" \\asc.upenn.edu\path\to\your\network_share /MIR

```
Using **FBTaskScheduler** We set this for 6AM & 8PM syncs to get before & after the work day changes!

- Finally, confirm with end user that daily OneDrive syncs are working as expected!

## TODO: 
- Rework this process for linux

    - Windows srv to linux os
    - batch file --> bash script
    - FBTaskScheduler --> crontab

- Figure out registry key for onedrive on LTSCs
