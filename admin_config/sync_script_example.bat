:: Author: Etienne P Jacquot - ASC IT SYSADMIN
:: Date: 2020-12-21
::
:: Sumamry: 
::	This batch file contains two examples for syncing all files from specified ASC Network Share to Microsoft Teams OneDrive synced on a virtual machine, 
::
:: 	You can create your own .bat file with one of the following commands, depending on your usecase:

::	 To copy FROM the network TO Teams OneDrive (for archiving to teams)
:: robocopy \\asc.upenn.edu\path\to\your\network_share "\\{NX_VMNAME}\C$\Users\{JANUS_USERNAME}\PennO365\{TEAM NAME & CHANNEL}" /MIR
	
:: 	To copy FROM Teams OneDrive TO the ASC network (for backups on prem))
:: robocopy "\\{NX_VMNAME}\C$\Users\{JANUS_USERNAME}\PennO365\{TEAM NAME & CHANNEL}" \\asc.upenn.edu\path\to\your\network_share /MIR

EXIT
