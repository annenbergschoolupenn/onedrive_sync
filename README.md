# OneDrive on ASC Win10 LTSC VM
## Etienne Jacquot (epj@asc.upenn.edu) - 12/21/2020 

### Getting Started with Installing OneDrive
1. Download OneDrive application [here](https://www.microsoft.com/en-us/microsoft-365/onedrive/download) on your ASC VM at [desktop.asc.upenn.edu](https://desktop.asc.upenn.edu).
    - Please close out of all O365 applications and click the downloaded executable to install OneDrive on your virtual machine. This may take a few minutes to complete...

2. Known issue: For some reason there is a *registry key* change that needs to be set in order for OneDrive to work on your virtual machine...
    - For reference, more info on this from a youtube video [here](https://www.youtube.com/watch?v=qITIddMUDpY)

- You must change the **DisableFileSyncNGSC** registry key from value *1* to **0**... 
    - *Start > search for RegEdit* (requires admin permission):  `HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive\DisableFileSyncNGSC`
    - Right click and **Modify** to set the Data value equal to 0:
``` regedit
Name: DisableFileSyncNGSC
Type: REG_DWORD
Data: 0 
```

3.  After setting this registry key, OneDrive should be successfully installed on your virtual machine. Please open OneDrive and authenticate with your PennO365 credentials (<your_pennkey>@upenn.edu) to login!
    - Once authenticated, you will notice that the OneDrive folder appears within FileExplorer, and by default your Teams chat history gets synced to your device. *If OneDrive does not show up automatically, please restart your VM.*

______________

### Configuring Teams & SharePoint online

4. Open Teams, navigate to *your Team > Files* and then select to *Open in Sharepoint* online 

![](https://imghost.asc.upenn.edu/onedrive/teams_sharepoint.PNG)

5. On the web, please select to **Sync** your Teams files to OneDrive on your virtual machine
    - Your browser may ask you to confirm this redirection to another app, click to allow
![](https://imghost.asc.upenn.edu/onedrive/sharepoint_online_sync.PNG)

6. Please wait for the OneDrive sync to take place. Once you see the files accessible in your File Explorer, you can *right click* and set to **Always keeps files on this device** to ensure content is always synced

![](https://imghost.asc.upenn.edu/onedrive/onedrive_fileexp.PNG)

______________

### Post Installation Steps (ASC Network Sync for OneDrive)

7. At this point you should have successfully synced your MS Teams files with your ASC Windows 10 virtual machine. 

8. Finally, please confirm & share with ASC IT the file path to your synced OneDrive folder on your VM. This information will enable us to configure daily robocopy's from your VMs folder to another folder on our network (for a backup copy!)

![](https://imghost.asc.upenn.edu/onedrive/od_filepath.PNG)

 - For example, this should be formatted probably something line:
 
 ```
 C:\Users\{JANUS_USERNAME}\PennO365\{TEAM NAME} - {TEAM CHANNEL}
 ```

- Fully qualified with computer hostname is formatted like (ASC IT will use this to setup a daily sync *from your VM to the network*)

``` 
\\NXVMName__\C$\Users\{JANUS_USERNAME}\PennO365\{TEAM NAME} - {TEAM CHANNEL}
```

### *PLEASE NOTE*:

It is important that you *do not log off your VM for an extended period of time!*
- If you are logged out for a while, the daily sync will *not* happen... 
- Of course if you reboot your VM for updates this is fine, just be sure to log back in.


______________

### ASC IT Administrator - Configuring batch sync for Teams OneDrive

Included in this repository is an example batch sync file [sync_script_example.bat](./sync_script_example.bat)
- This is for Windows batch script using the `robocopy` command (more info [here](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/robocopy))...

The sample .bat contains examples for configuring both types of transfers, make sure to uncomment the command (remove double ":") and finish the script with `EXIT`:

```batch
::	 To copy FROM the ASC Network TO Teams OneDrive (for archiving to teams)
:: robocopy \\asc.upenn.edu\path\to\your\network_share "\\{NX_VMNAME}\C$\Users\{JANUS_USERNAME}\PennO365\{TEAM NAME & CHANNEL}" /MIR
	
:: 	To copy FROM Teams OneDrive TO the ASC network (for backups on prem))
:: robocopy "\\{NX_VMNAME}\C$\Users\{JANUS_USERNAME}\PennO365\{TEAM NAME & CHANNEL}" \\asc.upenn.edu\path\to\your\network_share /MIR

```
Using **FBTaskScheduler** We set this for 6AM & 8PM syncs to get before & after the work day changes!

- Finally, confirm with end user that daily OneDrive syncs are working as expected!

## TODO: 
- Rework this process for linux

    - Windows srv to linux os
    - batch file --> bash script
    - FBTaskScheduler --> crontab

- Figure out registry key for onedrive on LTSCs
